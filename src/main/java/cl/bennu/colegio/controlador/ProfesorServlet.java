/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bennu.colegio.controlador;

import cl.bennu.colegio.dao.ColegioDAO;
import cl.bennu.colegio.dao.ProfesorDAO;
import cl.bennu.colegio.dto.AsignaturaEnum;
import cl.bennu.colegio.dto.ColegioDTO;
import cl.bennu.colegio.dto.ProfesorDTO;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alejandro
 */
@WebServlet(name = "ProfesorServlet", urlPatterns = {"/profesores"})
public class ProfesorServlet extends HttpServlet {

  private static final Logger log = Logger.getLogger(ProfesorServlet.class.getName());
  private final String PUT = "PUT";
  private final String DELETE = "DELETE";
  private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
  ProfesorDAO profesorDAO = new ProfesorDAO();
  ColegioDAO colegioDAO = new ColegioDAO();

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      List<ProfesorDTO> profesores = profesorDAO.buscarTodos();
      request.setAttribute("profesores", profesores);
      List<ColegioDTO> colegios = colegioDAO.buscarTodos();
      request.setAttribute("colegios", colegios);
      request.setAttribute("asignaturas", AsignaturaEnum.values());
      request.setAttribute("menu", "profesores");
      request.getRequestDispatcher("profesores.jsp").forward(request, response);
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      String method = request.getParameter("_method_");
      if (method != null) {
        switch (method) {
          case PUT:
            modificar(request, response);
            return;
          case DELETE:
            eliminar(request, response);
            return;
        }
      }
      agregar(request, response);
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
      request.setAttribute("error", "Error en la petición.");
    } finally {
      doGet(request, response);
    }
  }

  private void modificar(HttpServletRequest request, HttpServletResponse response) {
    try {
      String id = request.getParameter("id-edit");
      String nombre = request.getParameter("nombre-edit");
      String activo = request.getParameter("activo-edit");
      String fnac = request.getParameter("fnac-edit");
      String colegio = request.getParameter("colegio-edit");
      String asignatura = request.getParameter("asignatura-edit");
      boolean datosNulos = id == null || nombre == null || fnac == null || colegio == null || asignatura == null;
      boolean datosVacios = id.length() == 0 || nombre.length() == 0 || fnac.length() == 0 || colegio.length() == 0 || asignatura.length() == 0;
      if (datosNulos || datosVacios) {
        request.setAttribute("error", "Debe completar todos los datos para poder editar.");
        return;
      }
      Date fechaNac = new Date(df.parse(fnac).getTime());
      System.out.println(fnac);
      System.out.println(fechaNac);
      ProfesorDTO profesor = profesorDAO.buscar(Integer.parseInt(id));
      profesor.setNombre(nombre);
      profesor.setActivo(activo != null);
      profesor.setFechaNac(fechaNac);
      profesor.setColegio(colegioDAO.buscar(Integer.parseInt(colegio)));
      profesor.setAsignatura(AsignaturaEnum.valueOf(asignatura));
      profesorDAO.modificar(profesor);
      request.setAttribute("msg", "<strong>" + profesor.getNombre() + "</strong> modificado(a) correctamente.");
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
  }

  private void eliminar(HttpServletRequest request, HttpServletResponse response) {
    try {
      String id = request.getParameter("id-profesor");
      boolean datosNulos = id == null;
      boolean datosVacios = id.length() == 0;
      if (datosNulos || datosVacios) {
        request.setAttribute("error", "Error en el ID del Profesor.");
        return;
      }
      ProfesorDTO profesor = profesorDAO.buscar(Integer.parseInt(id));
      profesorDAO.eliminar(profesor.getId());
      request.setAttribute("msg", "<strong>" + profesor.getNombre() + "</strong> ha sido eliminado(a).");
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
      request.setAttribute("error", "Error en los datos, revise si el Profesor existe.");
    }
  }

  private void agregar(HttpServletRequest request, HttpServletResponse response) {
    try {
      String nombre = request.getParameter("nombre");
      String activo = request.getParameter("activo");
      String fnac = request.getParameter("fnac");
      String colegio = request.getParameter("colegio");
      String asignatura = request.getParameter("asignatura");
      boolean datosNulos = nombre == null || fnac == null || colegio == null || asignatura == null;
      if (datosNulos) {
        request.setAttribute("error", "Debe completar todos los datos.");
        return;
      }
      boolean datosVacios = nombre.length() == 0 || fnac.length() == 0 || colegio.length() == 0 || asignatura.length() == 0;
      if (datosVacios) {
        request.setAttribute("error", "Debe completar todos los datos.");
        return;
      }
      Date fechaNac = new Date(df.parse(fnac).getTime());
      ProfesorDTO profesor = new ProfesorDTO(
              (activo != null),
              AsignaturaEnum.valueOf(asignatura),
              fechaNac,
              nombre,
              colegioDAO.buscar(Integer.parseInt(colegio))
      );
      profesorDAO.agregar(profesor);
      request.setAttribute("msg", "<strong>" + profesor.getNombre() + "</strong> agregado(a) correctamente.");
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
  }

}
