<%-- 
    Document   : colegios
    Created on : 13-11-2018, 22:06:02
    Author     : Alejandro
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="${pageContext.servletContext.contextPath}/js/jquery.min.js" type="text/javascript"></script>
    <script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
    <title>Bennu</title>
  </head>
  <body>
    <%@include file="navbar.jsp" %>
    <div class="container">
      <h1>Mantenedor de Colegios</h1>
      <c:if test="${not empty error}">
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          ${error}
        </div>
      </c:if>
      <c:if test="${not empty msg}">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          ${msg}
        </div>
      </c:if>
      <br>
      <form class="form-horizontal" action="colegios" method="post">
        <div class="form-group">
          <label class="control-label col-sm-2" for="nombre">Nombre:</label>
          <div class="col-sm-10">
            <input maxlength="50" required type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre colegio">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="direccion">Direcci�n:</label>
          <div class="col-sm-10"> 
            <input maxlength="50" required type="text" class="form-control" id="direccion" name="direccion" placeholder="Direcci�n colegio">
          </div>
        </div>
        <div class="form-group"> 
          <div class="col-sm-offset-2 col-sm-10">
            <button type="reset" class="btn btn-warning">Limpiar</button>
            <button type="submit" class="btn btn-success">Guardar</button>
          </div>
        </div>
      </form>
      <div class="">
        <table class="table">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Direcci�n</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="c" items="${colegios}">
              <tr>
                <td>${c.getNombre()}</td>
                <td>${c.getDireccion()}</td>
                <td>
                  <button class="eliminar btn btn-danger btn-xs" data-id="${c.getId()}">Eliminar</button>
                  <button 
                    class="edit btn btn-primary btn-xs" 
                    data-nombre="${c.getNombre()}" 
                    data-direccion="${c.getDireccion()}" 
                    data-id="${c.getId()}">Editar</button>
                </td>
              </tr>
            </c:forEach>
          </tbody>
        </table>      
      </div>
    </div>
    <!--Modal Editar Colegio-->
    <div id="editColegio" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar Colegio</h4>
          </div>
          <form class="form-horizontal" action="colegios" method="post">
            <div class="modal-body">
              <input type="hidden" id="id-edit" name="id-edit"/>
              <input type="hidden" name="_method_" value="PUT"/>
              <div class="form-group">
                <label class="control-label col-sm-2" for="nombre">Nombre:</label>
                <div class="col-sm-10">
                  <input maxlength="50" required type="text" class="form-control" id="nombre-edit" name="nombre-edit" placeholder="Nombre colegio">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="direccion">Direcci�n:</label>
                <div class="col-sm-10"> 
                  <input maxlength="50" required type="text" class="form-control" id="direccion-edit" name="direccion-edit" placeholder="Direcci�n colegio">
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-sm-offset-6 col-sm-10">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-success">Modificar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Eliminar Colegio-->
    <div class="modal fade" id="eliminar" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Eliminar Colegio ?</h4>
          </div>
          <div class="modal-body">
            <p>�Desea elimnar <strong>Demo</strong>?</p>
          </div>
          <form action="colegios" method="post">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              <input type="hidden" id="id-colegio" name="id-colegio"/>
              <input type="hidden" name="_method_" value="DELETE"/>
              <button type="submit" class="btn btn-primary">Si, eliminar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <script>
      $(document).ready(function () {
        $('.edit').click(function () {
          var id = $(this).attr("data-id");
          var nombre = $(this).attr("data-nombre");
          var direccion = $(this).attr("data-direccion");
          $('#id-edit').val(id);
          $('#nombre-edit').val(nombre);
          $('#direccion-edit').val(direccion);
          $('#editColegio').modal('toggle');
        });
        $('.eliminar').click(function () {
          var id = $(this).attr("data-id");
          var nombre = $(this).closest('tr').find('td').first().text();
          $('#id-colegio').val(id);
          $('#eliminar').find('strong').text(nombre);
          $('#eliminar').modal('toggle');
        });
      });
    </script>
  </body>
</html>
