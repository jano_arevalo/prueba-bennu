/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bennu.colegio.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Alejandro
 */
public class Conexion {

  private static Conexion instance;

  private Connection cnn;

  public static Conexion getInstance() {
    if (instance == null) {
      instance = new Conexion();
    }
    return instance;
  }

  public Connection getCnn() {
    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
      String BaseDeDatos = "jdbc:mysql://lavegaonline.ml/bennu_bd?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false";
      if (cnn == null) {
        System.out.println("Se establece conexión");
        cnn = DriverManager.getConnection(BaseDeDatos, "bennu", "bennu");
      }
      return cnn;
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("error" + e);
      return null;
    }
  }

  public void desonectarBD() {
    try {
      cnn.close();
    } catch (SQLException ee) {
      System.out.println(ee.getMessage());
    }

  }
}
