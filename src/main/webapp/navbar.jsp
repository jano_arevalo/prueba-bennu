<%-- 
    Document   : navbar
    Created on : 13-11-2018, 23:12:48
    Author     : Alejandro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">B e n n u</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="${menu == "colegios" ? "active" : ""}"><a href="${menu == "colegios" ? "#" : "colegios"}">Colegios</a></li>
      <li class="${menu == "profesores" ? "active" : ""}"><a href="${menu == "profesores" ? "#" : "profesores"}">Profesores</a></li>
    </ul>
  </div>
</nav>