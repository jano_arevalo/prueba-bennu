<%-- 
    Document   : profesores
    Created on : 13-11-2018, 23:18:17
    Author     : Alejandro
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="${pageContext.servletContext.contextPath}/js/jquery.min.js" type="text/javascript"></script>
    <script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
    <title>Bennu</title>
  </head>
  <body>

    <%@include file="navbar.jsp" %>
    <div class="container">
      <h1>Mantenedor de Profesores</h1>
      <c:if test="${not empty error}">
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          ${error}
        </div>
      </c:if>
      <c:if test="${not empty msg}">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          ${msg}
        </div>
      </c:if>
      <br>
      <form class="form-horizontal" action="profesores" method="post">
        <div class="form-group">
          <label class="control-label col-sm-2" for="nombre">Nombre</label>
          <div class="col-sm-8">
            <input required type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre Profesor">
          </div>
          <div class="col-sm-2">
            <label class="checkbox-inline"><input required type="checkbox" name="activo">Activo</label>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="fnac">Fecha Nacimiento</label>
          <div class="col-sm-2">
            <input required type="date" class="form-control" id="fnac" name="fnac" placeholder="Fecha Nacimiento">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="colegio">Colegio</label>
          <div class="col-sm-10"> 
            <select required class="form-control" id="colegio" name="colegio">
              <c:forEach var="c" items="${colegios}">
                <option value="${c.getId()}">${c.getNombre()}</option>
              </c:forEach>
            </select>            
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="asignatura">Asignatura</label>
          <div class="col-sm-10"> 
            <select required class="form-control" id="asignatura" name="asignatura">
              <c:forEach var="c" items="${asignaturas}">
                <option value="${c}">${c}</option>
              </c:forEach>
            </select>            
          </div>
        </div>
        <div class="form-group"> 
          <div class="col-sm-offset-2 col-sm-10">
            <button type="reset" class="btn btn-warning">Limpiar</button>
            <button type="submit" class="btn btn-success">Guardar</button>
          </div>
        </div>
      </form>
      <div class="">
        <table class="table">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Fecha Nac.</th>
              <th>Colegio</th>
              <th>Asignatura</th>
              <th>Estado</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="p" items="${profesores}">
              <tr>
                <td>${p.getNombre()}</td>
                <td><fmt:formatDate value="${p.getFechaNac()}" pattern="yyyy-MM-dd"></fmt:formatDate></td>
                <td data-id="${p.getColegio().getId()}">${p.getColegio().getNombre()}</td>
                <td>${p.getAsignatura().toString()}</td>
                <td>${p.isActivo() ? "Activo" : "Inactivo"}</td>
                <td>
                  <button class="eliminar btn btn-danger btn-xs" data-id="${p.getId()}">Eliminar</button>
                  <button 
                    class="edit btn btn-primary btn-xs" 
                    data-id="${p.getId()}">Editar</button>
                </td>
              </tr>
            </c:forEach>
          </tbody>
        </table>      
      </div>
    </div>
    <!--Modal Editar Profesor-->
    <div id="editar" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar Profesor</h4>
          </div>
          <form class="form-horizontal" action="profesores" method="post">
            <div class="modal-body">
              <input type="hidden" id="id-edit" name="id-edit"/>
              <input type="hidden" name="_method_" value="PUT"/>
              <div class="form-group">
                <label class="control-label col-sm-2" for="nombre-edit">Nombre</label>
                <div class="col-sm-8">
                  <input  type="text" class="form-control" id="nombre-edit" name="nombre-edit" placeholder="Nombre Profesor">
                </div>
                <div class="col-sm-2">
                  <label class="checkbox-inline"><input type="checkbox" name="activo-edit" id="activo-edit">Activo</label>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="fnac-edit">Fecha Nacimiento</label>
                <div class="col-sm-4">
                  <input  type="date" class="form-control" id="fnac-edit" name="fnac-edit" placeholder="Fecha Nacimiento">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="colegio-edit">Colegio</label>
                <div class="col-sm-10"> 
                  <select class="form-control" id="colegio-edit" name="colegio-edit">
                    <c:forEach var="c" items="${colegios}">
                      <option value="${c.getId()}">${c.getNombre()}</option>
                    </c:forEach>
                  </select>            
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="asignatura-edit">Asignatura</label>
                <div class="col-sm-10"> 
                  <select class="form-control" id="asignatura-edit" name="asignatura-edit">
                    <c:forEach var="c" items="${asignaturas}">
                      <option value="${c}">${c}</option>
                    </c:forEach>
                  </select>            
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Eliminar Profesor-->
    <div class="modal fade" id="eliminar" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Eliminar Colegio ?</h4>
          </div>
          <div class="modal-body">
            <p>�Desea elimnar a <strong></strong>?</p>
          </div>
          <form action="profesores" method="post">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              <input type="hidden" id="id-profesor" name="id-profesor"/>
              <input type="hidden" name="_method_" value="DELETE"/>
              <button type="submit" class="btn btn-primary">Si, eliminar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <script>
      $(document).ready(function () {
        $('.edit').click(function () {
          var id = $(this).attr("data-id");
          var tr = $(this).closest('tr');
          var nombre = tr.find('td:eq(0)').text();
          var fnac = tr.find('td:eq(1)').text();
          var colegio = tr.find('td:eq(2)').attr('data-id');
          var asignatura = tr.find('td:eq(3)').text();
          var estado = tr.find('td:eq(4)').text() == 'Activo';
          $('#id-edit').val(id);
          $('#nombre-edit').val(nombre);
          $('#fnac-edit').val(fnac);
          $('#activo-edit').prop('checked', estado);
          $('#colegio-edit').val(colegio);
          $('#asignatura-edit').val(asignatura);
          $('#editar').modal('toggle');
        });
        $('.eliminar').click(function () {
          var id = $(this).attr("data-id");
          var nombre = $(this).closest('tr').find('td').first().text();
          $('#id-profesor').val(id);
          $('#eliminar').find('strong').text(nombre);
          $('#eliminar').modal('toggle');
        });
      });
    </script>
  </body>
</html>
