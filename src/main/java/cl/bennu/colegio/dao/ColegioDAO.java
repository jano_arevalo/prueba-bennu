/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bennu.colegio.dao;

import cl.bennu.colegio.dto.ColegioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alejandro
 */
public class ColegioDAO implements IMetodosCRUD<ColegioDTO> {

  private static final Logger log = ColegioDAO.log;
  private final String SQL_INSERT = "INSERT INTO colegio(nombre, direccion) VALUES(?, ?)";
  private final String SQL_UPDATE = "UPDATE colegio SET nombre = ?, direccion = ? WHERE id = ?";
  private final String SQL_DELETE = "DELETE FROM colegio WHERE id = ?";
  private final String SQL_SELECT = "SELECT id, nombre, direccion FROM colegio WHERE id = ?";
  private final String SQL_SELECT_ALL = "SELECT id, nombre, direccion FROM colegio";

  private Connection cnn;
  
  public ColegioDAO() {
    cnn = Conexion.getInstance().getCnn();
  }

  @Override
  public boolean agregar(ColegioDTO objeto) {
    boolean resp = false;
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_INSERT);
      ps.setString(1, objeto.getNombre());
      ps.setString(2, objeto.getDireccion());
      resp = ps.executeUpdate() == 1;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return resp;
  }

  @Override
  public ColegioDTO buscar(Object id) {
    ColegioDTO colegio = null;
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_SELECT);
      ps.setInt(1, (int) id);
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        colegio = new ColegioDTO(rs.getInt(1), rs.getString(2), rs.getString(3));
      }
      return colegio;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return colegio;
  }

  @Override
  public boolean modificar(ColegioDTO objeto) {
    boolean resp = false;
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_UPDATE);
      ps.setString(1, objeto.getNombre());
      ps.setString(2, objeto.getDireccion());
      ps.setInt(3, objeto.getId());
      resp = ps.executeUpdate() > 0;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return resp;
  }

  @Override
  public boolean eliminar(Object id) {
    boolean resp = false;
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_DELETE);
      ps.setInt(1, (int) id);
      resp = ps.executeUpdate() > 0;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return resp;
  }

  @Override
  public List<ColegioDTO> buscarTodos() {
    List<ColegioDTO> colegios = new ArrayList();
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_SELECT_ALL);
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        colegios.add(new ColegioDTO(rs.getInt(1), rs.getString(2), rs.getString(3)));
      }
      return colegios;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return colegios;
  }

}
