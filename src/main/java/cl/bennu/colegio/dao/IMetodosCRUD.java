/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bennu.colegio.dao;

import java.util.List;

/**
 *
 * @author Alejandro
 */
public interface IMetodosCRUD<T> {
  boolean agregar(T objeto);
  T buscar(Object id);
  boolean modificar(T objeto);
  boolean eliminar(Object id);
  List<T> buscarTodos();
  
}
