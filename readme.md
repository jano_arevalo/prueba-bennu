# B e n n u

# Prueba Selecci�n Desarrollador Java
---
## Prueba realizada para postular al cargo de desarrollador Java en **Bennu**.
## Se solicit� desarrollar un proyecto simple para un colegio, el cual consist�a en crear un mantenedor para colegios y profesores.
---
### Este proyecto se encuentra en una imagen Docker, la cual se puede obtener con el siguiente comando:
### `docker pull alejandroarevalo/prueba-bennu:1.0`

### Una vez descargada se puede crear un contenedor con el siguiente comando:
### `docker run --name <nombre_contenedor> -d -p 8080:8080 alejandroarevalo/prueba-bennu:1.0`

### Luego acceder a la siguiente url [http://192.168.99.100:8080](http://192.168.99.100:8080)
---