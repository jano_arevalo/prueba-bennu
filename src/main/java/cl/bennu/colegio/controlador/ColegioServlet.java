/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bennu.colegio.controlador;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cl.bennu.colegio.dao.ColegioDAO;
import cl.bennu.colegio.dto.ColegioDTO;

/**
 *
 * @author Alejandro
 */
@WebServlet(name = "ColegioServlet", urlPatterns = {"/colegios"})
public class ColegioServlet extends HttpServlet {

  private static final Logger log = Logger.getLogger(ColegioServlet.class.getName());
  private final String PUT = "PUT";
  private final String DELETE = "DELETE";
  ColegioDAO colegioDAO = new ColegioDAO();

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      List<ColegioDTO> colegios = colegioDAO.buscarTodos();
      request.setAttribute("menu", "colegios");
      request.setAttribute("colegios", colegios);
    } catch (Exception e) {
      request.setAttribute("error", "Error al consultar el servicio.");
      log.log(Level.SEVERE, e.getMessage());
    } finally {
      request.getRequestDispatcher("colegios.jsp").forward(request, response);
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      String method = request.getParameter("_method_");
      if (method != null) {
        switch (method) {
          case PUT:
            modificar(request, response);
            return;
          case DELETE:
            eliminar(request, response);
            return;
        }
      }
      agregar(request, response);
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
      request.setAttribute("error", "Error en la petición.");
    } finally {
      doGet(request, response);
    }
  }

  private void modificar(HttpServletRequest request, HttpServletResponse response) {
    try {
      String id = request.getParameter("id-edit");
      String nombre = request.getParameter("nombre-edit");
      String direccion = request.getParameter("direccion-edit");
      boolean datosNulos = id == null || nombre == null || direccion == null;
      boolean datosVacios = id.length() == 0 || nombre.length() == 0 || direccion.length() == 0;
      if (datosNulos || datosVacios) {
        request.setAttribute("error", "Complete todos los datos para poder editar.");
        return;
      }
      ColegioDTO colegio = colegioDAO.buscar(Integer.parseInt(id));
      colegio.setNombre(nombre);
      colegio.setDireccion(direccion);
      colegioDAO.modificar(colegio);
      request.setAttribute("msg", "<strong>" + colegio.getNombre() + "</strong> modificado correctamente.");
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
      request.setAttribute("error", "Error en los datos, revise si el colegio existe.");
    }
  }

  private void agregar(HttpServletRequest request, HttpServletResponse response) {
    try {
      String nombre = request.getParameter("nombre");
      String direccion = request.getParameter("direccion");
      boolean datosNulos = nombre == null || direccion == null;
      boolean datosVacios = nombre.length() == 0 || direccion.length() == 0;
      if (datosNulos || datosVacios) {
        request.setAttribute("error", "Complete todos los datos.");
        return;
      }
      ColegioDTO colegio = new ColegioDTO(nombre, direccion);
      colegioDAO.agregar(colegio);
      request.setAttribute("msg", "<strong>" + colegio.getNombre() + "</strong> agregado correctamente.");
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
      request.setAttribute("error", "Error en los datos, revise si el colegio existe.");
    }
  }

  private void eliminar(HttpServletRequest request, HttpServletResponse response) {
    try {
      String id = request.getParameter("id-colegio");
      boolean datosNulos = id == null;
      boolean datosVacios = id.length() == 0;
      if (datosNulos || datosVacios) {
        request.setAttribute("error", "Error en el ID del colegio.");
        return;
      }
      ColegioDTO colegio = colegioDAO.buscar(Integer.parseInt(id));
      colegioDAO.eliminar(colegio.getId());
      request.setAttribute("msg", "<strong>" + colegio.getNombre() + "</strong> ha sido eliminado correctamente.");
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
      request.setAttribute("error", "No puede eliminar un colegio que tiene profesores.");
    }
  }

}
