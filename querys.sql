--a.	Consulta que obtenga todos los profesores de un colegio ordenados por nombre.
select id, nombre, fecha_nac, asignatura, activo, colegio_id from profesor where colegio_id = 1 order by nombre asc;

--b.	Consulta que obtenga todos los alumnos con promedio rojo por asignatura.
select alumno_id, avg(nota) as promedio, asignatura_id from nota group by asignatura_id, alumno_id having promedio < 4;

--c.	Consulta que contenga el alumno con mejor nota por asignatura.
select n.alumno_id, n.nota, n.asignaura_id from nota n inner join (select max(nota) as notaMax, asignatura_id from nota group by asignatura_id) nmax on n.nota = nmax.notaMax and n.asignatura_id = nmax.asignatura_id;