/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bennu.colegio.dto;

import java.sql.Date;

/**
 *
 * @author Alejandro
 */
public class ProfesorDTO {
  private int id;
  private boolean activo;
  private AsignaturaEnum asignatura;
  private Date fechaNac;
  private String nombre;
  private ColegioDTO colegio;

  public ProfesorDTO(boolean activo, AsignaturaEnum asignatura, Date fechaNac, String nombre, ColegioDTO colegio) {
    this.activo = activo;
    this.asignatura = asignatura;
    this.fechaNac = fechaNac;
    this.nombre = nombre;
    this.colegio = colegio;
  }

  public ProfesorDTO(int id, boolean activo, AsignaturaEnum asignatura, Date fechaNac, String nombre, ColegioDTO colegio) {
    this.id = id;
    this.activo = activo;
    this.asignatura = asignatura;
    this.fechaNac = fechaNac;
    this.nombre = nombre;
    this.colegio = colegio;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public boolean isActivo() {
    return activo;
  }

  public void setActivo(boolean activo) {
    this.activo = activo;
  }

  public AsignaturaEnum getAsignatura() {
    return asignatura;
  }

  public void setAsignatura(AsignaturaEnum asignatura) {
    this.asignatura = asignatura;
  }

  public Date getFechaNac() {
    return fechaNac;
  }

  public void setFechaNac(Date fechaNac) {
    this.fechaNac = fechaNac;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public ColegioDTO getColegio() {
    return colegio;
  }

  public void setColegio(ColegioDTO colegio) {
    this.colegio = colegio;
  }
  
}
