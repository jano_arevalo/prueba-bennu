-- Prueba de selección para postular al cargo de desarrollador Java en Bennu

-- Tabla: alumno
CREATE TABLE alumno (
    id int NOT NULL AUTO_INCREMENT,
    nombre varchar(50) NOT NULL,
    fecha_nac date NOT NULL,
    colegio_id int NOT NULL,
    CONSTRAINT alumno_pk PRIMARY KEY (id)
);

-- Tabla: asignatura
CREATE TABLE asignatura (
    id int NOT NULL AUTO_INCREMENT,
    nombre varchar(50) NOT NULL,
    CONSTRAINT asignatura_pk PRIMARY KEY (id)
);

-- Tabla: colegio
CREATE TABLE colegio (
    id int NOT NULL AUTO_INCREMENT,
    nombre varchar(50) NOT NULL,
    direccion varchar(50) NOT NULL,
    CONSTRAINT colegio_pk PRIMARY KEY (id)
);

-- Tabla: nota
CREATE TABLE nota (
    id int NOT NULL AUTO_INCREMENT,
    nota double(2,1) NOT NULL,
    alumno_id int NOT NULL,
    asignatura_id int NOT NULL,
    CONSTRAINT nota_pk PRIMARY KEY (id)
);

-- Tabla: profesor
CREATE TABLE profesor (
    id int NOT NULL AUTO_INCREMENT,
    nombre varchar(50) NOT NULL,
    fecha_nac date NOT NULL,
    asignatura varchar(50) NOT NULL,
    activo bool NOT NULL,
    colegio_id int NOT NULL,
    CONSTRAINT profesor_pk PRIMARY KEY (id)
);

-- claves foráneas
ALTER TABLE alumno ADD CONSTRAINT alumno_colegio FOREIGN KEY alumno_colegio (colegio_id) REFERENCES colegio (id);

ALTER TABLE nota ADD CONSTRAINT nota_alumno FOREIGN KEY nota_alumno (alumno_id) REFERENCES alumno (id);

ALTER TABLE nota ADD CONSTRAINT nota_asignatura FOREIGN KEY nota_asignatura (asignatura_id) REFERENCES asignatura (id);

ALTER TABLE profesor ADD CONSTRAINT profesor_colegio FOREIGN KEY profesor_colegio (colegio_id) REFERENCES colegio (id);