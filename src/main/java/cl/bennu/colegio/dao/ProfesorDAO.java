/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bennu.colegio.dao;

import cl.bennu.colegio.dto.AsignaturaEnum;
import cl.bennu.colegio.dto.ProfesorDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alejandro
 */
public class ProfesorDAO implements IMetodosCRUD<ProfesorDTO> {

  private static final Logger log = ProfesorDAO.log;
  private final String SQL_INSERT = "INSERT INTO profesor(activo, asignatura, fecha_nac, nombre, colegio_id) VALUES(?, ?, ?, ?, ?)";
  private final String SQL_UPDATE = "UPDATE profesor SET activo = ?, asignatura = ?, fecha_nac = ?, nombre = ?, colegio_id = ? WHERE id = ?";
  private final String SQL_DELETE = "DELETE FROM profesor WHERE id = ?";
  private final String SQL_SELECT = "SELECT id, activo, asignatura, fecha_nac, nombre, colegio_id FROM profesor WHERE id = ?";
  private final String SQL_SELECT_ALL = "SELECT id, activo, asignatura, fecha_nac, nombre, colegio_id FROM profesor";

  private Connection cnn;

  public ProfesorDAO() {
    cnn = Conexion.getInstance().getCnn();
  }

  @Override
  public boolean agregar(ProfesorDTO objeto) {
    boolean resp = false;
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_INSERT);
      ps.setBoolean(1, objeto.isActivo());
      ps.setString(2, objeto.getAsignatura().toString());
      ps.setDate(3, objeto.getFechaNac());
      ps.setString(4, objeto.getNombre());
      ps.setInt(5, objeto.getColegio().getId());
      resp = ps.executeUpdate() == 1;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return resp;
  }

  @Override
  public ProfesorDTO buscar(Object id) {
    ColegioDAO colegioDao = new ColegioDAO();
    ProfesorDTO profesor = null;
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_SELECT);
      ps.setInt(1, (int) id);
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        profesor = new ProfesorDTO(
                rs.getInt(1),
                rs.getBoolean(2),
                AsignaturaEnum.valueOf(rs.getString(3)),
                rs.getDate(4),
                rs.getString(5),
                colegioDao.buscar(rs.getInt(6))
        );
      }
      return profesor;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return profesor;
  }

  @Override
  public boolean modificar(ProfesorDTO objeto) {
    boolean resp = false;
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_UPDATE);
      ps.setBoolean(1, objeto.isActivo());
      ps.setString(2, objeto.getAsignatura().toString());
      ps.setDate(3, objeto.getFechaNac());
      ps.setString(4, objeto.getNombre());
      ps.setInt(5, objeto.getColegio().getId());
      ps.setInt(6, objeto.getId());
      resp = ps.executeUpdate() > 0;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return resp;
  }

  @Override
  public boolean eliminar(Object id) {
    boolean resp = false;
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_DELETE);
      ps.setInt(1, (int) id);
      resp = ps.executeUpdate() > 0;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return resp;
  }

  @Override
  public List<ProfesorDTO> buscarTodos() {
    ColegioDAO colegioDao = new ColegioDAO();
    List<ProfesorDTO> profesores = new ArrayList();
    try {
      PreparedStatement ps = cnn.prepareStatement(SQL_SELECT_ALL);
      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        profesores.add(
                new ProfesorDTO(
                        rs.getInt(1),
                        rs.getBoolean(2),
                        AsignaturaEnum.valueOf(rs.getString(3)),
                        rs.getDate(4),
                        rs.getString(5),
                        colegioDao.buscar(rs.getInt(6))
                ));
      }
      return profesores;
    } catch (Exception e) {
      log.log(Level.SEVERE, e.getMessage());
    }
    return profesores;
  }

}
